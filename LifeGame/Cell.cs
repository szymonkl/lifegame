﻿using System;
using System.Collections.Generic;

namespace LifeGame
{
    public class Cell : ICloneable
    {
        public string Id { get; set; }
        public int XValue { get; set; }
        public int YValue { get; set; }
        public bool NewState { get; set; }
        public bool OldState { get; set; }
        public List<Cell> Neighbours { get; set; }
        public List<Cell> ActiveNeighbours { get; set; }
        public int NeighboursCount { get; set; }
        public int ActiveNeighboursCount { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}