﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LifeGame
{
    public class Game
    {
        public List<Cell> Array { get; set; }

        public List<Cell> ActiveCells { get; set; }
        public void Initialize(int columns, int rows)
        {
            Array = new List<Cell>();
            ActiveCells = new List<Cell>();
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    Cell cell = new Cell
                    {
                        XValue = j,
                        YValue = i,
                        Id = i + j.ToString(),
                        OldState = Calculatios.GenerateState()
                    };
                    Array.Add(cell);
                    if (cell.OldState) ActiveCells.Add(cell);
                }
            }
            Array.ForEach(c => Calculatios.GetNeighbours(Array, c));
            Array.ForEach(Calculatios.GetActiveNeighbours);
        }

        public void Update()
        {
                ActiveCells = new List<Cell>();
                foreach (var cell in Array)
                {
                    Calculatios.GetActiveNeighbours(cell);
                    int neighboursCount = cell.ActiveNeighboursCount;
                    if (cell.OldState == false)
                    {
                        if (neighboursCount == 3)
                        {
                            cell.NewState = true;
                            ActiveCells.Add(cell);

                        }
                    }
                    else
                    {
                        if (neighboursCount == 2 || neighboursCount == 3)
                        {
                            cell.NewState = true;
                            ActiveCells.Add(cell);
                        }
                        else
                        {
                            cell.NewState = false;
                        }
                    }
                
            }
                Array.ForEach(c => c.OldState = c.NewState);

        }



    }


    internal static class Calculatios
    {
        public static bool GenerateState()
        {
            Guid guid = Guid.NewGuid();
            int number = guid.GetHashCode();
            return number % 2 == 0;
        }

        public static void GetNeighbours(List<Cell> array, Cell cell)
        {
            List<Cell> neighbours = new List<Cell>();

            foreach (var currentCell in array)
            {
                if (Math.Abs(currentCell.XValue - cell.XValue) < 2 &&
                    Math.Abs(currentCell.YValue - cell.YValue) < 2)
                {
                    if (currentCell.Id != cell.Id)
                    {
                        neighbours.Add(currentCell);
                        if (neighbours.Count == 8) break;
                    }
                }
            }
            cell.Neighbours = neighbours;
            cell.NeighboursCount = neighbours.Count;
            
        }

        public static void GetActiveNeighbours(Cell cell)
        {
            cell.ActiveNeighbours = cell.Neighbours.FindAll(c => c.OldState);
            cell.ActiveNeighboursCount = cell.ActiveNeighbours.Count;
        }
    }
}
