﻿using System;

namespace LifeGame
{
    class Program
    {
        private static System.Timers.Timer _timer = new System.Timers.Timer();
        private static Game _game;
        static void Main(string[] args)
        {
            _timer.Interval = 1000;
            _timer.Elapsed += _timer_Elapsed;
            _game = new Game();
            _game.Initialize(3,3);
            GUI.Draw(_game);
            Console.WriteLine();
            _timer.Start();
            
            //GUI.DrawNeighbours(game);
            //Console.WriteLine();
            //GUI.DrawActiveNeighbours(game);
            //Console.WriteLine();
            

            Console.ReadLine();
        }

        private static void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            
            _game.Update();
            GUI.Draw(_game);
            Console.WriteLine();
            
        }
    }
}
