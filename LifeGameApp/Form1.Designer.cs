﻿namespace LifeGameApp
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title3 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblAll = new System.Windows.Forms.Label();
            this.lblAllValue = new System.Windows.Forms.Label();
            this.lblLive = new System.Windows.Forms.Label();
            this.lblLiveValue = new System.Windows.Forms.Label();
            this.lblIterations = new System.Windows.Forms.Label();
            this.lblIterationsValue = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lblMaxLive = new System.Windows.Forms.Label();
            this.lblMinLive = new System.Windows.Forms.Label();
            this.lblMaxLiveValue = new System.Windows.Forms.Label();
            this.lblMinLiveValue = new System.Windows.Forms.Label();
            this.lblPercentage = new System.Windows.Forms.Label();
            this.lblPercentageValue = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblTimeValue = new System.Windows.Forms.Label();
            this.lblDrawingTime = new System.Windows.Forms.Label();
            this.lblDrawingTimeValue = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(83, 269);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(161, 43);
            this.button1.TabIndex = 1;
            this.button1.Text = "Generate/Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(9, 9);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(202, 198);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // lblAll
            // 
            this.lblAll.AutoSize = true;
            this.lblAll.Location = new System.Drawing.Point(12, 218);
            this.lblAll.Name = "lblAll";
            this.lblAll.Size = new System.Drawing.Size(27, 17);
            this.lblAll.TabIndex = 3;
            this.lblAll.Text = "All:";
            // 
            // lblAllValue
            // 
            this.lblAllValue.AutoSize = true;
            this.lblAllValue.Location = new System.Drawing.Point(45, 218);
            this.lblAllValue.Name = "lblAllValue";
            this.lblAllValue.Size = new System.Drawing.Size(16, 17);
            this.lblAllValue.TabIndex = 4;
            this.lblAllValue.Text = "0";
            // 
            // lblLive
            // 
            this.lblLive.AutoSize = true;
            this.lblLive.Location = new System.Drawing.Point(97, 218);
            this.lblLive.Name = "lblLive";
            this.lblLive.Size = new System.Drawing.Size(38, 17);
            this.lblLive.TabIndex = 5;
            this.lblLive.Text = "Live:";
            // 
            // lblLiveValue
            // 
            this.lblLiveValue.AutoSize = true;
            this.lblLiveValue.Location = new System.Drawing.Point(141, 218);
            this.lblLiveValue.Name = "lblLiveValue";
            this.lblLiveValue.Size = new System.Drawing.Size(16, 17);
            this.lblLiveValue.TabIndex = 6;
            this.lblLiveValue.Text = "0";
            // 
            // lblIterations
            // 
            this.lblIterations.AutoSize = true;
            this.lblIterations.Location = new System.Drawing.Point(193, 218);
            this.lblIterations.Name = "lblIterations";
            this.lblIterations.Size = new System.Drawing.Size(70, 17);
            this.lblIterations.TabIndex = 7;
            this.lblIterations.Text = "Iterations:";
            // 
            // lblIterationsValue
            // 
            this.lblIterationsValue.AutoSize = true;
            this.lblIterationsValue.Location = new System.Drawing.Point(269, 218);
            this.lblIterationsValue.Name = "lblIterationsValue";
            this.lblIterationsValue.Size = new System.Drawing.Size(16, 17);
            this.lblIterationsValue.TabIndex = 8;
            this.lblIterationsValue.Text = "0";
            // 
            // chart1
            // 
            chartArea3.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea3);
            this.chart1.Location = new System.Drawing.Point(473, 12);
            this.chart1.Name = "chart1";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Name = "Live Cells";
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(309, 315);
            this.chart1.TabIndex = 9;
            this.chart1.Text = "chart1";
            title3.Name = "Title1";
            title3.Text = "Live cells per iterations";
            this.chart1.Titles.Add(title3);
            // 
            // lblMaxLive
            // 
            this.lblMaxLive.AutoSize = true;
            this.lblMaxLive.Location = new System.Drawing.Point(12, 244);
            this.lblMaxLive.Name = "lblMaxLive";
            this.lblMaxLive.Size = new System.Drawing.Size(62, 17);
            this.lblMaxLive.TabIndex = 10;
            this.lblMaxLive.Text = "Max live:";
            // 
            // lblMinLive
            // 
            this.lblMinLive.AutoSize = true;
            this.lblMinLive.Location = new System.Drawing.Point(116, 244);
            this.lblMinLive.Name = "lblMinLive";
            this.lblMinLive.Size = new System.Drawing.Size(59, 17);
            this.lblMinLive.TabIndex = 11;
            this.lblMinLive.Text = "Min live:";
            // 
            // lblMaxLiveValue
            // 
            this.lblMaxLiveValue.AutoSize = true;
            this.lblMaxLiveValue.Location = new System.Drawing.Point(72, 244);
            this.lblMaxLiveValue.Name = "lblMaxLiveValue";
            this.lblMaxLiveValue.Size = new System.Drawing.Size(16, 17);
            this.lblMaxLiveValue.TabIndex = 12;
            this.lblMaxLiveValue.Text = "0";
            // 
            // lblMinLiveValue
            // 
            this.lblMinLiveValue.AutoSize = true;
            this.lblMinLiveValue.Location = new System.Drawing.Point(181, 244);
            this.lblMinLiveValue.Name = "lblMinLiveValue";
            this.lblMinLiveValue.Size = new System.Drawing.Size(16, 17);
            this.lblMinLiveValue.TabIndex = 13;
            this.lblMinLiveValue.Text = "0";
            // 
            // lblPercentage
            // 
            this.lblPercentage.AutoSize = true;
            this.lblPercentage.Location = new System.Drawing.Point(220, 244);
            this.lblPercentage.Name = "lblPercentage";
            this.lblPercentage.Size = new System.Drawing.Size(110, 17);
            this.lblPercentage.TabIndex = 14;
            this.lblPercentage.Text = "Percent live [%]:";
            // 
            // lblPercentageValue
            // 
            this.lblPercentageValue.AutoSize = true;
            this.lblPercentageValue.Location = new System.Drawing.Point(336, 244);
            this.lblPercentageValue.Name = "lblPercentageValue";
            this.lblPercentageValue.Size = new System.Drawing.Size(16, 17);
            this.lblPercentageValue.TabIndex = 15;
            this.lblPercentageValue.Text = "0";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(220, 9);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(118, 17);
            this.lblTime.TabIndex = 16;
            this.lblTime.Text = "Update time [ms]:";
            // 
            // lblTimeValue
            // 
            this.lblTimeValue.AutoSize = true;
            this.lblTimeValue.Location = new System.Drawing.Point(344, 9);
            this.lblTimeValue.Name = "lblTimeValue";
            this.lblTimeValue.Size = new System.Drawing.Size(44, 17);
            this.lblTimeValue.TabIndex = 17;
            this.lblTimeValue.Text = "0.000";
            // 
            // lblDrawingTime
            // 
            this.lblDrawingTime.AutoSize = true;
            this.lblDrawingTime.Location = new System.Drawing.Point(220, 35);
            this.lblDrawingTime.Name = "lblDrawingTime";
            this.lblDrawingTime.Size = new System.Drawing.Size(123, 17);
            this.lblDrawingTime.TabIndex = 18;
            this.lblDrawingTime.Text = "Drawing time [ms]:";
            // 
            // lblDrawingTimeValue
            // 
            this.lblDrawingTimeValue.AutoSize = true;
            this.lblDrawingTimeValue.Location = new System.Drawing.Point(344, 35);
            this.lblDrawingTimeValue.Name = "lblDrawingTimeValue";
            this.lblDrawingTimeValue.Size = new System.Drawing.Size(44, 17);
            this.lblDrawingTimeValue.TabIndex = 19;
            this.lblDrawingTimeValue.Text = "0.000";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 339);
            this.Controls.Add(this.lblDrawingTimeValue);
            this.Controls.Add(this.lblDrawingTime);
            this.Controls.Add(this.lblTimeValue);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.lblPercentageValue);
            this.Controls.Add(this.lblPercentage);
            this.Controls.Add(this.lblMinLiveValue);
            this.Controls.Add(this.lblMaxLiveValue);
            this.Controls.Add(this.lblMinLive);
            this.Controls.Add(this.lblMaxLive);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.lblIterationsValue);
            this.Controls.Add(this.lblIterations);
            this.Controls.Add(this.lblLiveValue);
            this.Controls.Add(this.lblLive);
            this.Controls.Add(this.lblAllValue);
            this.Controls.Add(this.lblAll);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblAll;
        private System.Windows.Forms.Label lblAllValue;
        private System.Windows.Forms.Label lblLive;
        private System.Windows.Forms.Label lblLiveValue;
        private System.Windows.Forms.Label lblIterations;
        private System.Windows.Forms.Label lblIterationsValue;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Label lblMaxLive;
        private System.Windows.Forms.Label lblMinLive;
        private System.Windows.Forms.Label lblMaxLiveValue;
        private System.Windows.Forms.Label lblMinLiveValue;
        private System.Windows.Forms.Label lblPercentage;
        private System.Windows.Forms.Label lblPercentageValue;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblTimeValue;
        private System.Windows.Forms.Label lblDrawingTime;
        private System.Windows.Forms.Label lblDrawingTimeValue;
    }
}

