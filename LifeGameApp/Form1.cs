﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using LifeGame;

namespace LifeGameApp
{
    public partial class Form1 : Form
    {
        private Graphics _graphics;
        private Pen _pen;
        private Game _game;
        private Timer _timer;
        public Form1()
        {
            InitializeComponent();
            _game = new Game();
          
            _graphics = pictureBox1.CreateGraphics();
            _timer = new Timer();
            _timer.Interval = 900;
            _timer.Tick += _timer_Tick;
            _timer.Start();
        }

        private void _timer_Tick(object sender, EventArgs e)
        {
            if (_isClicked)
            {
                pictureBox1.Refresh();
                DateTime calculationsStart = DateTime.Now;
                _game.Update();
                DateTime calculationsStop = DateTime.Now;
                lblTimeValue.Text = (calculationsStop - calculationsStart).TotalMilliseconds.ToString("0.00");
                DateTime drawingStart = DateTime.Now;
                foreach (var cell in _game.ActiveCells)
                {
                    _graphics.DrawRectangle(_pen, cell.XValue, cell.YValue, 1, 1);
                }
                DateTime drawingStop = DateTime.Now;
                lblDrawingTimeValue.Text = (drawingStop - drawingStart).TotalMilliseconds.ToString("0.00");
                lblLiveValue.Text = _game.ActiveCells.Count.ToString();
                _iterations++;
                lblIterationsValue.Text = _iterations.ToString();
                chart1.Series[0].Points.AddXY(_iterations, _game.ActiveCells.Count);
                lblPercentageValue.Text = (Convert.ToDouble(100*_game.ActiveCells.Count) / _game.Array.Count).ToString("0.000");
                if (_maxLive < _game.ActiveCells.Count)
                {
                    lblMaxLiveValue.Text = _game.ActiveCells.Count.ToString();
                    _maxLive = _game.ActiveCells.Count;
                }
                if (_minLive > _game.ActiveCells.Count)
                {
                    lblMinLiveValue.Text = _game.ActiveCells.Count.ToString();
                    _minLive = _game.ActiveCells.Count;
                }


            }
            
        }

        bool _isClicked = false;
        private int _iterations = 0;
        private int _maxLive = 0;
        private int _minLive = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            lblAllValue.Text = (pictureBox1.Width * pictureBox1.Height).ToString();

            if (!_isClicked)
            {
                _game = new Game();
                _game.Initialize(pictureBox1.Width, pictureBox1.Height);
                _pen = new Pen(Color.Black);
                _isClicked = true;
            }
            else
            {
                pictureBox1.Refresh();
                DateTime calculationsStart = DateTime.Now;
                _game.Update();
                DateTime calculationsStop = DateTime.Now;
                lblTimeValue.Text = (calculationsStop - calculationsStart).TotalMilliseconds.ToString("0.00");
              
            }

            DateTime drawingStart = DateTime.Now;
            foreach (var cell in _game.ActiveCells)
            { 
                _graphics.DrawRectangle(_pen, cell.XValue, cell.YValue, 1,1);
                
            }
            DateTime drawingStop = DateTime.Now;
            lblDrawingTimeValue.Text = (drawingStop - drawingStart).TotalMilliseconds.ToString("0.00");
            lblLiveValue.Text = _game.ActiveCells.Count.ToString();
            _iterations++;
            _maxLive = _game.ActiveCells.Count;
            _minLive = _game.ActiveCells.Count;
            lblIterationsValue.Text = _iterations.ToString();
            lblMaxLiveValue.Text = _maxLive.ToString();
            lblMinLiveValue.Text = _minLive.ToString();
            lblPercentageValue.Text = (100*Convert.ToDouble(_game.ActiveCells.Count) / _game.Array.Count).ToString("0.00");
            chart1.Series[0].Points.AddXY(_iterations, _game.ActiveCells.Count);
        }
    }
}
